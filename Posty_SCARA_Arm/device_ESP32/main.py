#----------------------- 
# notify
#-----------------------

print('RUN: main.py') 

#----------------------- 
# safety catch
#-----------------------

import time
for x in range(10):
    print('CATCH',x+1)
    for y in range(10):
        time.sleep_ms(10)
print('READY')

#-----------------------
# notes
#-----------------------

#-----------------------
# configuration values
#-----------------------

# motors
m1e,m1s,m1d =  5,18,15
m2e,m2s,m2d = 21,22,33 
m3e,m3s,m3d = 23,25,26 

#-----------------------
# general imports
#-----------------------

# this will run
#import posty_esp32_a4899

#-----------------------
# run
#-----------------------

def run():
    pass

#-----------------------
# testing run
#-----------------------

def run():

    print('RUN')

    global m1,m2,m3

    print('RUN2')

    from steppers import A4899

    print('RUN3')
##
##    m1 = A4899(m1s,m1d,m1e)
##    print('RUN31')
##    m2 = A4899(m2s,m2d,m2e)
##    print('RUN32')
    m3 = A4899(m3s,m3d,m3e)
    print('RUN33')

    print('RUN4')

##    print('M1 step 20',end=' ')
##    m1.wake()
##    m1.step(20)
##    print('done')
##    print('M1 step -20',end=' ')
##    m1.step(-20)
##    m1.sleep()
##    print('done')

##    m2.wake()
##    print('M2 step 20',end=' ')
##    m2.step(20)
##    print('done')
##    print('M2 step -20',end=' ')
##    m2.step(-20)
##    print('done')

    m3.wake()
    print('M3 step 20',end=' ')
    m3.step(20)
    print('done')
    print('M3 step -20',end=' ')
    m3.step(-20)
    print('done')

    

##    m1.sleep()
##    m2.sleep()
    m3.sleep()

    



    
    pass
#-----------------------
# run on start
#-----------------------

if __name__ == '__main__':
    run()

#-----------------------
# end
#-----------------------
