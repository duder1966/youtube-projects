#----------------------- 
# notify
#-----------------------

print('RUN: main.py') 

#----------------------- 
# safety catch
#-----------------------

import time
for x in range(100):
    time.sleep_ms(10)

#-----------------------
# notes
#-----------------------

#-----------------------
# configuration values
#-----------------------

#-----------------------
# general imports
#-----------------------

import os
import sys
import time

from machine import Pin

#-----------------------
# run
#-----------------------

def run():
    pass

#-----------------------
# run on start
#-----------------------

if __name__ == '__main__':
    run()

#-----------------------
# end
#-----------------------
