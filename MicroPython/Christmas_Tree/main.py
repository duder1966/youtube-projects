#-----------------------
# notify
#-----------------------

print('RUN: main.py')

#-----------------------
# imports
#-----------------------

import time
from pixels import PIXELS
from random import randint,choice

#-----------------------
# user setup
#-----------------------

# pixels gpio pin
pixel_pin = 14

# pixels on string
pixel_count = 139

# brightness percent
brightness = 32

#-----------------------
# pixels
#-----------------------

# set up pixels (global)
pixels = PIXELS(pixel_pin,pixel_count)
pixels.off()
print('PIXELS READY')

# fixed colors for later use
r = pixels.make_color('red',int(brightness/2))
g = pixels.make_color('green',int(brightness/2))
b = pixels.make_color('blue',int(brightness/2))
y = pixels.make_color('yellow',int(brightness))
z = (0,0,0)

def clear():
    for x in range(pixel_count):
        pixels.np[x] = (0,0,0)
    
def push(c1,c2,c3,pause=0):
    for x in range(pixel_count):
        clear()
        pixels.np[x] = c1
        for p in range(x-5,-1,-10):
            if p >= 0:
                pixels.np[p] = c2
                if p >= 5:
                    pixels.np[p-5] = c3
        pixels.np.write()
        time.sleep_ms(pause)

def pull(pause=15):
    for x in range(pixel_count-1,-1,-1):
        new = pixels.np.buf[3:] + bytearray((0,0,0))
        pixels.np.buf = new 
        pixels.np.write()
        time.sleep_ms(pause)

def up1():
    top = pixels.np.buf[-3:]
    end = pixels.np.buf[-6:-3]
    mid = pixels.np.buf[:-6]
    pixels.np.buf = end + mid + top
    pixels.np.write()

def down1():
    top = pixels.np.buf[-3:]
    end = pixels.np.buf[:3]
    mid = pixels.np.buf[3:-3]
    pixels.np.buf = mid + end + top
    pixels.np.write()
        








##def load():
##    for x in range(0,pixel_count,10):
##        pixels.np[x] = r
##        pixels.np.write()
##        time.sleep_ms(3)
##    for x in range(5,pixel_count,10):
##        pixels.np[x] = g
##        pixels.np.write()
##        time.sleep_ms(3)

##def up1():
##    end = pixels.np.buf[-3:]
##    pixels.np.buf = end + pixels.np.buf[:-3]
##    pixels.np.write()

##def down1():
##    end = pixels.np.buf[:3]
##    pixels.np.buf = pixels.np.buf[3:]+end
##    pixels.np.write()

#-----------------------
# main
#-----------------------

def main():

    try:
        
        while 1:

            select = randint(1,10)
            if select >= 10:
                c1,c2,c3 = y,y,y
                #loops = 1
                loops = randint(1,2)
            elif select >= 8:
                c1,c2,c3 = b,b,b
                #loops = 1
                loops = randint(2,4)
            else:
                c1,c2,c3 = y,g,r
                loops = randint(4,6)

            push(c1,c2,c3,6)
            time.sleep_ms(1000)

            for loop in range(loops):

                accs =  60 # acceleration cycles
                hold =  30 # hold cycles
                decs =  60 # deceleration cycles
                mint =  25 # min step time ms
                maxt = 115 # max step time ms
                zero =   4 # hold at zero maxt multiplier

                fa = int(1000*(maxt-mint)/accs)
                fd = int(1000*(maxt-mint)/decs)
                mint *= 1000

                # accelerate up
                for x in range(accs):
                    t = (accs-x)*fa + mint
                    up1()
                    time.sleep_us(t)

                # hold up
                for x in range(hold):
                    up1()
                    time.sleep_us(t)

                # decelerate up
                for x in range(decs,-1,-1):
                    t = (accs-x)*fd + mint
                    up1()
                    time.sleep_us(t)

                # pause at 0
                time.sleep_us(t*zero)
 
                # accelerate down
                for x in range(accs):
                    t = (accs-x)*fa + mint
                    down1()
                    time.sleep_us(t)

                # hold down
                for x in range(hold):
                    down1()
                    time.sleep_us(t)

                # quit here on last loop
                if loop == loops-1:
                    break

                # decelerate down
                for x in range(decs,-1,-1):
                    t = (accs-x)*fd + mint
                    down1()
                    time.sleep_us(t)

                # pause at 0
                time.sleep_us(t*zero)

            pull(int(t/1000))

            time.sleep_ms(1000)

    except Exception as e:
        import sys
        sys.print_exception(e)

    finally:
        pixels.off()


#-----------------------
# run on start
#-----------------------

if __name__ == "__main__":
    main()
    
#-----------------------
# end
#-----------------------




##def main():
##
##    # loop
##    while 1:
##
##        # flash about 4 seconds
##        print('PIXELS FLASH')
##        pixels.rblink('red green sun'.split(),pixels=8,times=100,brightness=brightness,ontime=50,offtime=0)
##        pixels.off()
##        
##        # base load
##        load()
##        pixels.off()

        



### 2nd version of main
### same as 1st, but longer periods
##def main2():
##
##    # set up pixels (global)
##    pixels = PIXELS(pixel_pin,pixel_count)
##    pixels.off()
##    print('PIXELS READY')
##
##    # fixed colors for later use
##    r = pixels.make_color('red',int(brightness/2))
##    g = pixels.make_color('green',int(brightness/2))
##    z = (0,0,0)
##
##    # run 2 directions
##    print('PIXELS R-G RUN')
##    for x in range(pixel_count):
##        pixels.np[x] = r
##        pixels.np[pixel_count-1-x] = g
##        pixels.np.write()
##        time.sleep_ms(10)
##        pixels.np[x] = z
##        pixels.np[pixel_count-1-x] = z
##        pixels.np.write()            
##    pixels.off()
##
##    # loop
##    while 1:
##
##        # flash about 4 seconds
##        print('PIXELS FLASH')
##        pixels.rblink('red green sun'.split(),pixels=4,times=91,brightness=brightness,ontime=44,offtime=0)
##        pixels.off()
##
##        # follow up for about 26 seconds
##        print('PIXELS RUN UP')
##        for x in range(0,pixel_count,10):
##            pixels.np[x] = r
##        for x in range(5,pixel_count,10):
##            pixels.np[x] = g
##        pixels.np.write()
##        for x in range(260):
##            end = pixels.np.buf[-3:]
##            pixels.np.buf = end + pixels.np.buf[:-3]
##            pixels.np.write()
##            time.sleep_ms(100)
##        pixels.off()
##
##        # flash about 4 seconds
##        print('PIXELS FLASH')
##        pixels.rblink('red green sun'.split(),pixels=4,times=91,brightness=brightness,ontime=44,offtime=0)
##        pixels.off()
##
##        # follow down for about 26 seconds
##        print('PIXELS RUN DOWN')
##        for x in range(0,pixel_count,10):
##            pixels.np[x] = r
##        for x in range(5,pixel_count,10):
##            pixels.np[x] = g
##        pixels.np.write()
##        for x in range(260):
##            end = pixels.np.buf[:3]
##            pixels.np.buf = pixels.np.buf[3:]+end
##            pixels.np.write()
##            time.sleep_ms(100)
##        pixels.off()
