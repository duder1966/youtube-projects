#-----------------------
# notify
#-----------------------

# Stolen from Clayton Darwin

print('LOAD: nanopix.py')

#-----------------------
# imports
#-----------------------

import sys,time
from machine import Pin
from neopixel import NeoPixel

#-----------------------
# nanopix class
#-----------------------

class NANOPIX:

    #-----------------------
    # layout
    #-----------------------

    # pixel configuration for 1 panel
    #
    #  in --->  0  1  2  3  4  5  6  7
    #          15 14 13 12 11 10  9  8
    #          16 17 18 19 20 21 22 23
    #          31 30 29 28 27 26 25 24
    #          32 33 34 35 36 37 38 39
    #          47 46 45 44 43 42 41 40
    #          48 49 50 51 52 53 54 55
    # out <--- 63 62 61 60 59 58 57 56

    # panel configuration for NX by NY panels
    #
    #         |---|      |---|      |---|
    # in ---> | 1 | ---> | 2 | ---> |NX | ---> 
    #         |---|      |---|      |---|     |
    #                                         |
    #    -------------------------------------
    #   |
    #   |     |---|      |---|      |---|
    #    ---> | 1 | ---> | 2 | ---> |NY | ---> out
    #         |---|      |---|      |---| 
    #
    
    #-----------------------
    # variables
    #-----------------------

    # data pin
    pin = 21
    pob = None # pin object

    # neopixel objects
    xpanels = 1 # x panels
    ypanels = 1 # y panels 
    pixels = 64 # total pixels
    xpixels = 8 # x pixel width
    ypixels = 8 # y pixel height
    np = None # neopixel object

    # pixel buffer
    pbuf = None

    #-----------------------
    # init
    #-----------------------

    # init
    def __init__(self,pin=None,xpanels=1,ypanels=1):

        # pin
        if pin != None:
            self.pin = pin
        self.p = Pin(self.pin,Pin.OUT)

        # pixel counts
        self.xpanels = xpanels
        self.ypanels = ypanels
        self.pixels  = 64 * xpanels * ypanels
        self.xpixels = 8 * xpanels
        self.ypixels = 8 * ypanels

        # pixel object
        self.np = NeoPixel(self.p,self.pixels)

    #-----------------------
    # colors and brightness
    #-----------------------

    # brightness out of 255
    brightness = 16

    # GRB bold colors
    colors = {

        'black':(0,0,0),
        'white':(255,255,225),

        'green':(255,0,0),
        'greener':(255,32,0),
        'lime':(255,64,0),
        'chartreuse':(255,128,0),
        'yellow':(225,255,0),
        'sunflower':(128,255,0),
        'orange':(64,255,0),
        'pumpkin':(32,255,0),
        'tomato':(16,255,0),
        'red':(0,255,0),
        'rose':(0,255,16),
        'fuchsia':(0,255,32),
        'magenta':(0,255,64),
        'pinker':(0,255,128),
        'pink':(0,255,255),
        'violet':(0,128,255),

        'ultra':(0,64,255),
        'indigo':(0,32,255),
        'blue':(0,0,255),
        'water':(32,0,255),
        'sky':(64,0,255),
        'azure':(128,0,255),
        'cyan':(255,0,255),
        'aqua':(255,0,128),

        'mint':(255,0,64),
        'grass':(255,0,32),
        }

    # brightness reset
    def set_brightness(self,brightness=0):
        self.brightness = min(255,abs(brightness))

    # make/select a color
    def make_color(self,color,brightness=None):

        # zero == off
        if brightness == 0:
            return (0,0,0)

        # tuple value
        if type(color) in (list,tuple):
            color = (list(color)+[0,0,0])[:3]
            if brightness:
                brightness = min(255,abs(brightness))
                color = [int(x*brightness/255) for x in color]
            return tuple(color)

        # text value
        v = self.colors.get(color,(255,0,0))
        b = brightness or self.brightness
        return bytearray([int(x*b/255) for x in v])

    #-----------------------
    # base functions
    #-----------------------
    
    # clear values (don't write)
    # np.fill() is faster than indexing
    # re-defining buffer is fastest
    def clear(self):
        self.np.buf = bytearray(self.pixels*3)

    # write
    def write(self):
        self.np.write()

    # off == clear and write
    def off(self):
        self.clear()
        self.write()

    #-----------------------
    # xy indexing 
    #-----------------------

    # (0,0) is the top left pixel

    # calculating is faster then using a lookup table
    def xy2i(self,x,y):
        # pixels for rows above current row
        i  = (y//8) * self.xpixels * 8
        # pixels for panels left of target panel
        i += (x//8) * 64
        # pixels for rows above target row
        i += (y%8)*8
        # even rows (0,2,4,6) -->
        if not y % 2:
            i += x%8
        # odd rows (1,3,5,7) <--
        else:
            i += 7-x%8
        # done
        return i*3

    def getxy(self,x,y):
        # check x value
        if 0 <= x < self.xpixels:
            # check y value
            if 0 <= y < self.ypixels:
                i = self.xy2i(x,y)
                return tuple(self.np.buf[i:i+3])
        return None

    def setxy(self,x,y,color='red',brightness=None,rgb=None):
        # check x value
        if 0 <= x < self.xpixels:
            # check y value
            if 0 <= y < self.ypixels:
                i = self.xy2i(x,y)
                self.np.buf[i:i+3] = rgb or self.make_color(color,brightness)

                
##                # get
##                i = self.xy2i(x,y)
##                # set
##                self.np.buf[i:i+3] = rgb or self.make_color(color,brightness)
##                # placed
##                return i
##        # not placed
##        return None

#-----------------------
# end
#-----------------------
    

##    # RGB bold colors
##    colors = {
##        'red':    (255,  0,  0),
##        'orange': (255, 16,  0),
##        'sun':    (255, 64,  0),
##        'yellow': (255,160,  0),
##        'lime':   ( 64,255,  0),
##        'green':  (  0,255,  0),
##        'mint':   (  0,255, 16),
##        'cyan':   (  0,255, 96),
##        'sky':    (  0,255,255),
##        'blue':   (  0,  0,255),
##        'purple': (128, 0, 255),
##        'magenta':(255, 0, 255),
##        'pink':   (255, 0, 128),
##
##        'white':  (255,255,192),
##
##        'black':  (  0,  0,  0),
##        'off':    (  0,  0,  0),
##        }

##    # index table lookup from x,y
##    xy2it = []

##        # make index table
##        self.make_xy2it()

##    # build a table of xy indexes
##    def make_xy2it(self):
##        print('BUILD XY2IT')
##        self.xy2it = []
##        for x in range(self.xpixels):
##            self.xy2it.append([self.xy2i(x,y) for y in range(self.ypixels)])
##        print('XY2IT: {}x{}'.format(len(self.xy2it),len(self.xy2it[0])))
##
##        self.xy2it2 = {}
##        for x in range(self.xpixels):
##            for y in range(self.ypixels):
##                self.xy2it2[x*y] = self.xy2i(x,y)
##        print('XY2IT2:',len(self.xy2it2))
        
##    def setxy(self,x,y,color='red',brightness=None,rgb=None):
##        # check x value
##        if 0 <= x < self.xpixels:
##            # check y value
##            if 0 <= y < self.ypixels:
##                # get
##                i = self.xy2it[x][y]
##                # set
##                self.np[i] = rgb or self.make_color(color,brightness)
##                # placed
##                return i
##        # not placed
##        return None

##    def getxy(self,x,y):
##        # check x value
##        if 0 <= x < self.xpixels:
##            # check y value
##            if 0 <= y < self.ypixels:
##                # get index
##                i = self.xy2it[x][y]
##                # get value
##                return self.np[i]
##        # not placed
##        return None

##    def getxy2(self,x,y):
##        # check x value
##        if 0 <= x < self.xpixels:
##            # check y value
##            if 0 <= y < self.ypixels:
##                # get index
##                i = self.xy2it2.get(x*y,0)
##                # get value
##                return self.np[i]
##        # not placed
##        return None

