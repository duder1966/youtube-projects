#-----------------------
# imports
#-----------------------

import sys
import time

#-----------------------
# notes
#-----------------------

#-----------------------
# wifi class
#-----------------------

class WLAN:

    #-----------------------
    # variables
    #-----------------------

    essid = None
    password = None
    timezone = None
    tzoffset = 0
    last_time_update = 0

    #-----------------------
    # init
    #-----------------------

    def __init__(self):

        # wlan
        from network import WLAN as _WLAN
        from network import STA_IF as _STA_IF
        self.wlan = _WLAN(_STA_IF)
        del _WLAN,_STA_IF
        
        # rtc
        from machine import RTC
        self.rtc = RTC()
        del RTC    

    #-----------------------
    # functions
    #-----------------------

    def scan(self):
        state = self.wlan.active() # save current state
        self.wlan.active(True) # set state active
        from ubinascii import hexlify
        for ssid,bssid,channel,RSSI,authmode,hidden in self.wlan.scan():
            ssid = ssid.decode('ascii')
            bssid = hexlify(bssid).decode('ascii')
            if len(bssid) == 12:
                bssid = ':'.join([bssid[x:x+2] for x in range(0,12,2)])
            authmode = ('OPEN','WEP','WPA-PSK','WPA2-PSK','WPA/WPA2-PSK')[min(4,max(0,authmode))]
            if hidden:
                hidden = True
            else:
                False
            print('Network AP:',[ssid,bssid,channel,RSSI,authmode,hidden])
        self.wlan.active(state) # return to previous state
        del hexlify

    def ip(self,full=False):
        if full:
            return self.wlan.ifconfig()
        return self.wlan.ifconfig()[0]

    def isconnected(self):
        if self.wlan.isconnected():
            return True
        return False        

    def connect(self,essid=None,password=None,timeout=15):
        essid = essid or self.essid
        password = password or self.password
        print('Network Connect:',essid)
        self.wlan.active(True)
        if not self.wlan.isconnected():
            self.wlan.connect(essid,password)
            time.sleep_ms(100)
            for x in range(timeout):
                if self.wlan.isconnected():
                    break
                time.sleep_ms(500)
        if self.wlan.isconnected():
            print('Network Connect:',essid,'OK',self.wlan.ifconfig()[0])
            return True
        print('Network Connect:',essid,'FAILED')
        return False

    def disconnect(self,timeout=15):
        print('Network Disconnect')
        return_value = True
        if self.wlan.active():
            if self.wlan.isconnected():
                self.wlan.disconnect()
                time.sleep_ms(100)
                for x in range(timeout):
                    if not self.wlan.isconnected():
                        break
                    time.sleep_ms(1000)
                return_value = not self.wlan.isconnected()
        self.wlan.active(False)
        print('Network Disonnect:',return_value)
        return return_value

    def set_time_with_thread(self,tries=3,tz=None):
        import _thread
        _thread.start_new_thread(self.set_time,(tries,tz))

    def set_time(self,tries=3,tz=None):
        try:
            self._set_time(tries,tz)
        except Exception as e:
            sys.print_exception(e)
            print('SET TIME FAILED')

    def _set_time(self,tries=3,tz=None):

        # connect
        stay_connected = True
        if not self.isconnected():
            stay_connected = False
            self.connect()

        # ntp time
        ntpok = self.set_ntptime(tries)

        # timezone time
        tzok = True
        if ntpok and (tz or self.timezone):
            tzok = self.set_tzoffset(tries,tz)

        # disconnect
        if not stay_connected:
            self.disconnect()

        # success
        if ntpok and tzok:
            self.last_time_update = time.time()
            return True
        return False

    def set_ntptime(self,tries=3):
        ntpok = False
        from ntptime import settime as _settime
        for x in range(tries):
            try:
                _settime()
                ntpok = True
                break
            except Exception as e:
                print('NTP TIME ERROR',x+1)
                time.sleep_ms(100)
        del _settime
        if ntpok:
            print('RTC NTP OK')
        else:
            print('RTC NTP FAILED')
        return ntpok

    def set_tzoffset(self,tries=3,tz=None):
        tzok = False
        tz = tz or self.timezone
        if tz:
            import urequests
            for x in range(tries):
                try:
                    resp = urequests.get(url='http://cammo.pro/cgi-bin/ztime.py?TZ=New_York')
                    # resp.text = '2025 01 10 18 12 59 EST UTC-0500\n'
                    hours = int(resp.text.strip().split()[3])-time.gmtime()[3]
                    if hours > 0:
                        hours -= 24
                    self.tzoffset = 3600 * hours
                    print('TZ OFFSET OK',hours)
                    tzok = True
                    break
                except Exception as e:
                    print('TZ OFFSET ERROR',x+1)
                    time.sleep_ms(100)
            del urequests
            if tzok:
                print('SET TZ OFFSET OK')
            else:
                print('SET TZ OFFSET FAILED')
        return tzok

    def localtime(self):
        # see "not well documented" in self.set
        # this returns local time based on self.tzoffset
        # this returns a tuple (year,month,day,hours,minutes,seconds)
        # rtc.datetime()  == (year, month, day, weekday, hours, minutes, seconds, subseconds)
        # time.gmtime() == (year, month, mday, hour, minute, second, weekday, yearday)
        return time.gmtime(time.time()+self.tzoffset)[:6]

    @property
    def epoch(self):
        # return linux epoch
        # embedded epoch is 2000-01-01
        # offset from linux epoch (1970)
        # time.gmtime(946684800) == time.struct_time(tm_year=2000, tm_mon=1, tm_mday=1, tm_hour=0, tm_min=0, tm_sec=0, tm_wday=5, tm_yday=1, tm_isdst=0)
        return int(time.time()+946684800)
        
    @property
    def dtstamp(self):
        return '{:0>4}-{:0>2}-{:0>2} {:0>2}:{:0>2}:{:0>2}'.format(*self.localtime())

    @property
    def ampm(self):
        _,_,_,h,m,_ = self.localtime()
        t = 'AM'
        if h == 0:
            h = 12
        elif h == 12:
            t = 'PM'
        elif h > 12:
            h -= 12
            t = 'PM'
        return '{}:{:0>2}{}'.format(h,m,t)
        

#-----------------------
# end
#-----------------------
