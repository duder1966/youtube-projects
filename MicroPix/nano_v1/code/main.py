#----------------------- 
# notify
#-----------------------

print('RUN: main.py') 

#----------------------- 
# safety catch
#-----------------------

import time
for x in range(10):
    print('SAFETY CATCH',x)
    for y in range(10):
        time.sleep_ms(10)

#-----------------------
# notes
#-----------------------

#-----------------------
# configuration values
#-----------------------

#-----------------------
# general imports
#-----------------------

import time

#-----------------------
# run
#-----------------------

def run():
    pass

from wlan import WLAN
wlan = WLAN()
#wlan.scan()
wlan.essid = 'DARWIN-NET-3'
wlan.password = 'claytondarwin'
wlan.timezone = 'New_York'
wlan.connect()

from nanopix import NANOPIX
np = NANOPIX(21,10,1)

from nanofont import NANOFONT
nf = NANOFONT(np)

text1,tlen1 = nf.fix_text('00:00')
color1 = np.make_color('red',8)

text2,tlen2 = nf.fix_text('Acculon')
text3,tlen3 = nf.fix_text('Energy')
color2 = np.make_color('pumpkin',8)

# wait function
last = time.ticks_us()
def wait(wtms=60):
    global last
    while time.ticks_diff(time.ticks_us(),last) < (wtms*1000):
        time.sleep_us(100)
    last = time.ticks_us()

while 1:

    # time check
    if not wlan.last_time_update:
        print('TIME INIT')
        np.off()
        wlan.set_time(10)
    elif time.time() - wlan.last_time_update >= 25*3600:
        print('TIME EXPIRED')
        np.off()
        wlan.set_time(10)
    elif wlan.localtime()[3] == 0 and time.time() - wlan.last_time_update >= 3600:
        print('TIME UPDATE THREAD')
        wlan.set_time_with_thread(10)

##    # time display 2 rotations
##    for c in range(2):
##        for x in range(np.xpixels-1,-1,-1):
##            text1 = wlan.ampm
##            nf.np.clear()
##            nf.place_text(text1,x,0,rgb=color1,overflowx=True)
##            nf.place_text(text1,x+40,0,rgb=color1,overflowx=True)
##            np.np.write()
##            wait()

    # go down to acculon
    # uses x range 0-8
    ty = 0
    ay = -9
    for x in range(np.xpixels-1,np.xpixels-10,-1):
        ty += 1
        ay += 1
        nf.np.clear()
        nf.place_text(text2,x   ,ay,rgb=color2,overflowx=True)
        nf.place_text(text3,x+42,ay,rgb=color2,overflowx=True)
        text1 = wlan.ampm
        nf.place_text(text1,x   ,ty,rgb=color1,overflowx=True)
        nf.place_text(text1,x+40,ty,rgb=color1,overflowx=True)
        np.np.write()
        wait()
        

    # finish acculon loop
    for x in range(np.xpixels-10,-1,-1):
        nf.np.clear()
        nf.place_text(text2,x,0,rgb=color2,overflowx=True)
        nf.place_text(text3,x+42,0,rgb=color2,overflowx=True)
        np.np.write()
        wait()

##    # acculon 2 rotations
##    for c in range(2):
##        for x in range(np.xpixels-1,-1,-1):
##            nf.np.clear()
##            nf.place_text(text2,x,0,rgb=color2,overflowx=True)
##            nf.place_text(text3,x+42,0,rgb=color2,overflowx=True)
##            np.np.write()
##            wait()
            
    # go up to time display
    # uses x range 0-8
    ty = 9
    ay = 0
    for x in range(np.xpixels-1,np.xpixels-1-9,-1):
        ty -= 1
        ay -= 1
        nf.np.clear()
        nf.place_text(text2,x   ,ay,rgb=color2,overflowx=True)
        nf.place_text(text3,x+42,ay,rgb=color2,overflowx=True)
        text1 = wlan.ampm
        nf.place_text(text1,x   ,ty,rgb=color1,overflowx=True)
        nf.place_text(text1,x+40,ty,rgb=color1,overflowx=True)
        np.np.write()
        wait()

    # finish time loop
    for x in range(np.xpixels-1-9,-1,-1):
        nf.np.clear()
        text1 = wlan.ampm
        nf.place_text(text1,x   ,ty,rgb=color1,overflowx=True)
        nf.place_text(text1,x+40,ty,rgb=color1,overflowx=True)
        np.np.write()
        wait()

#-----------------------
# run on start
#-----------------------

if __name__ == '__main__':
    run()

#-----------------------
# end
#-----------------------
